/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.kantinan.finalproject3;

/**
 *
 * @author Acer
 */
public class Order {
    private String descrirtion;
    private  String size;
    private String color;
    private  String sleevesize;
    private  String screenyourtext;
    private  int number;
    private  String name;
    private int phonenumber;
    private  String address;
    private  String payment;

    public Order(String descrirtion, String size, String color, String sleevesize, String screenyourtext, int number, String name, int phonenumber, String address, String payment) {
        this.descrirtion = descrirtion;
        this.size = size;
        this.color = color;
        this.sleevesize = sleevesize;
        this.screenyourtext = screenyourtext;
        this.number = number;
        this.name = name;
        this.phonenumber = phonenumber;
        this.address = address;
        this.payment = payment;
    }

    public String getDescrirtion() {
        return descrirtion;
    }

    public void setDescrirtion(String descrirtion) {
        this.descrirtion = descrirtion;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSleevesize() {
        return sleevesize;
    }

    public void setSleevesize(String sleevesize) {
        this.sleevesize = sleevesize;
    }

    public String getScreenyourtext() {
        return screenyourtext;
    }

    public void setScreenyourtext(String screenyourtext) {
        this.screenyourtext = screenyourtext;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(int phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    @Override
    public String toString() {
        return "FinalProject3{" + "descrirtion=" + descrirtion + ", size=" + size + ", color=" + color + ", sleevesize=" + sleevesize + ", screenyourtext=" + screenyourtext + ", number=" + number + ", name=" + name + ", phonenumber=" + phonenumber + ", address=" + address + ", payment=" + payment + '}';
    }
}
